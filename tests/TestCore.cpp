// Eigenx is a extension of the Eigen library that aims to have simpler syntax and faster build times.
//
// Copyright (C) 2014-2017 Allan Leal
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <doctest/doctest.hpp>

// Eigenx includes
#include <Eigenx/Core.hpp>
using namespace Eigen;

TEST_CASE("Testing Core module.")
{
    MatrixXd mat = random(5, 5);

    VectorXi ind = {2, 4};

    MatrixXd res = rows(mat, ind);

    auto rmap = rowsmap(mat, 2, 3);
    auto cmap = colsmap(mat, 3, 2);
    auto bmap = blockmap(mat, 2, 2, 2, 3);

    CHECK(res.row(0).isApprox(mat.row(2)));
    CHECK(res.row(1).isApprox(mat.row(4)));

    CHECK(mat.middleRows(2, 3).isApprox(rmap));
    CHECK(mat.middleCols(3, 2).isApprox(cmap));
    CHECK(mat.block(2, 2, 2, 3).isApprox(bmap));

    // Checking if range-for works
    for(auto& i : ind)
        i *= 2;

    CHECK(ind.isApprox(VectorXi{4, 8}));
}

