# Include the current directory
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

# Find Python, Numpy and Boost.Python libraries
find_package(PythonLibs 2.7 REQUIRED)
find_package(Numpy 2.7 REQUIRED)
find_package(Boost COMPONENTS python REQUIRED)

# Specify Python, Numpy and Boost include directories
include_directories(${PYTHON_INCLUDE_DIRS})
include_directories(${NUMPY_INCLUDE_DIR})
include_directories(${Boost_INCLUDE_DIRS})

# Collect the header and source files of PyEigenx
file(GLOB_RECURSE HEADER_FILES *.hpp)
file(GLOB_RECURSE SOURCE_FILES *.cpp)

# Suppress pedantic warnings related to ISO C++ for GNU compiler
if(${CMAKE_COMPILER_IS_GNUCXX})
    add_definitions(-Wno-pedantic -Wno-unused-local-typedefs)
endif()

# Create the PyEigenx library
add_library(PyEigenx SHARED ${HEADER_FILES} ${SOURCE_FILES})

# Link PyEigenx against Boost.Python and Python libraries
target_link_libraries(PyEigenx ${Boost_LIBRARIES} ${PYTHON_LIBRARIES})

# Remove the `lib` prefix of the PyEigenx library file name
set_target_properties(PyEigenx PROPERTIES PREFIX "" OUTPUT_NAME eigenx)

# Check if PyEigenx library is output as `.dll` (Windows) and change it to `.pyd`
if(${CMAKE_SHARED_LIBRARY_SUFFIX} MATCHES ".dll")
    set_target_properties(PyEigenx PROPERTIES SUFFIX ".pyd")
endif()

# Check if PyEigenx library is output as `.dylib` (MacOS) and change it to `.so`
if(${CMAKE_SHARED_LIBRARY_SUFFIX} MATCHES ".dylib")
    set_target_properties(PyEigenx PROPERTIES SUFFIX ".so")
endif()

# Create an install target for PyEigenx
install(TARGETS PyEigenx DESTINATION "lib" COMPONENT interfaces)
