// Eigenx is a extension of the Eigen library that aims to have simpler syntax and faster build times.
//
// Copyright (C) 2014-2017 Allan Leal
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// Boost includes
#include <boost/python.hpp>
namespace py = boost::python;

// PyEigenx includes
#include "PyEigen.hpp"
using namespace Eigen;

BOOST_PYTHON_MODULE(eigenx)
{
    // Set numpy as the numeric::array engine
    py::numeric::array::set_module_and_type("numpy", "ndarray");

    // Customize the docstring options
    py::docstring_options docstring_options;
    docstring_options.disable_cpp_signatures();
    docstring_options.enable_py_signatures();
    docstring_options.enable_user_defined();

    // The following export order matters
    export_Eigen();
}
