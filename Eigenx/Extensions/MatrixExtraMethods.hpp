// Eigenx is a extension of the Eigen library that aims to have simpler syntax and faster build times.
//
// Copyright (C) 2014-2017 Allan Leal
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Eigenx/Eigen/Core>

namespace Eigen {

template<typename T>
using MatrixX = Matrix<T, Dynamic, Dynamic>;

template<typename T>
using VectorX = Matrix<T, Dynamic, 1>;

template<typename T>
using RowVectorX = Matrix<T, 1, Dynamic>;

/// Return an expression of a zero vector.
/// @param rows The number of rows
template<typename T = double, typename Index>
auto zeros(Index rows) -> decltype(VectorX<T>::Zero(rows))
{
    return VectorX<T>::Zero(rows);
}

/// Return an expression of a vector with entries equal to one.
/// @param rows The number of rows
template<typename T = double, typename Index>
auto ones(Index rows) -> decltype(VectorX<T>::Ones(rows))
{
    return VectorX<T>::Ones(rows);
}

/// Return an expression of a vector with entries equal to a given constant.
/// @param rows The number of rows
/// @param val The constant value
template<typename T = double, typename U, typename Index>
auto constants(Index rows, const U& val) -> decltype(VectorX<T>::Constant(rows, val))
{
    return VectorX<T>::Constant(rows, val);
}

/// Return an expression of a vector with random entries.
/// @param rows The number of rows
template<typename T = double, typename Index>
auto random(Index rows) -> decltype(VectorX<T>::Random(rows))
{
    return VectorX<T>::Random(rows);
}

/// Return a linearly spaced vector.
/// @param rows The number of rows
/// @param start The start of the sequence
/// @param stop The stop of the sequence
template<typename T = double, typename U, typename V, typename Index>
auto linspace(Index rows, const U& start, const V& stop) -> decltype(VectorX<T>::LinSpaced(rows, start, stop))
{
    return VectorX<T>::LinSpaced(rows, start, stop);
}

/// Return a linearly spaced vector from zero to `rows` - 1.
/// @param rows The number of rows
template<typename T = double, typename Index>
auto linspace(Index rows) -> decltype(VectorX<T>::LinSpaced(rows, 0, rows))
{
    return VectorX<T>::LinSpaced(rows, 0, rows);
}

/// Return an expression of a unit vector.
/// @param rows The number of rows
/// @param i The index at which the vector component is one
template<typename T = double, typename Index>
auto unit(Index rows, Index i) -> decltype(VectorX<T>::Unit(rows, i))
{
    return VectorX<T>::Unit(rows, i);
}

/// Return an expression of a matrix with entries equal to zero.
/// @param rows The number of rows
/// @param cols The number of columns
template<typename T = double, typename Index>
auto zeros(Index rows, Index cols) -> decltype(MatrixX<T>::Zero(rows, cols))
{
    return MatrixX<T>::Zero(rows, cols);
}

/// Return an expression of a matrix with entries equal to one.
/// @param rows The number of rows
/// @param cols The number of columns
template<typename T = double, typename Index>
auto ones(Index rows, Index cols) -> decltype(MatrixX<T>::Ones(rows, cols))
{
    return MatrixX<T>::Ones(rows, cols);
}

/// Return an expression of a vector with entries equal to a given constant.
/// @param rows The number of rows
/// @param val The constant value
template<typename T = double, typename U, typename Index>
auto constants(Index rows, Index cols, const U& val) -> decltype(MatrixX<T>::Constant(rows, cols, val))
{
    return MatrixX<T>::Constant(rows, cols, val);
}

/// Return an expression of a matrix with random entries.
/// @param rows The number of rows
/// @param cols The number of columns
template<typename T = double, typename Index>
auto random(Index rows, Index cols) -> decltype(MatrixX<T>::Random(rows, cols))
{
    return MatrixX<T>::Random(rows, cols);
}

/// Return an expression of an identity matrix.
/// @param rows The number of rows
/// @param cols The number of columns
template<typename T = double, typename Index>
auto identity(Index rows, Index cols) -> decltype(MatrixX<T>::Identity(rows, cols))
{
    return MatrixX<T>::Identity(rows, cols);
}

/// Return a transpose expression of a matrix.
template<typename Derived>
auto tr(MatrixBase<Derived>& mat) -> decltype(mat.transpose())
{
    return mat.transpose();
}

/// Return a const transpose expression of a matrix.
template<typename Derived>
auto tr(const MatrixBase<Derived>& mat) -> decltype(mat.transpose())
{
    return mat.transpose();
}

/// Return an inverse expression of a matrix.
template<typename Derived>
auto inv(const MatrixBase<Derived>& mat) -> decltype(mat.cwiseInverse())
{
    return mat.cwiseInverse();
}

/// Return a diagonal matrix expression of a vector.
template<typename Derived>
auto diag(const MatrixBase<Derived>& vec) -> decltype(vec.asDiagonal())
{
    return vec.asDiagonal();
}

/// Return a vector expression of a matrix diagonal.
template<typename Derived>
auto diagonal(MatrixBase<Derived>& mat) -> decltype(mat.diagonal())
{
    return mat.diagonal();
}

/// Return a const vector expression of a matrix diagonal.
template<typename Derived>
auto diagonal(const MatrixBase<Derived>& mat) -> decltype(mat.diagonal())
{
    return mat.diagonal();
}

/// Return the Lp norm of a matrix.
template<int p, typename Derived>
auto norm(const MatrixBase<Derived>& mat) -> decltype(mat.template lpNorm<p>())
{
    return mat.template lpNorm<p>();
}

/// Return the L2 norm of a matrix.
template<typename Derived>
auto norm(const MatrixBase<Derived>& mat) -> decltype(mat.norm())
{
    return mat.norm();
}

/// Return the L-inf norm of a matrix.
template<typename Derived>
auto norminf(const MatrixBase<Derived>& mat) -> decltype(mat.template lpNorm<Infinity>())
{
    return mat.template lpNorm<Infinity>();
}

/// Return the sum expression of the entries of a matrix.
template<typename Derived>
auto sum(const DenseBase<Derived>& mat) -> decltype(mat.sum())
{
    return mat.sum();
}

/// Return the dot product expression of two matrices.
template<typename DerivedLHS, typename DerivedRHS>
auto dot(const MatrixBase<DerivedLHS>& lhs, const MatrixBase<DerivedRHS>& rhs) -> decltype(lhs.dot(rhs))
{
    return lhs.dot(rhs);
}

/// Return the minimum entry of a matrix.
template<typename Derived>
auto min(const MatrixBase<Derived>& mat) -> decltype(mat.minCoeff())
{
    return mat.minCoeff();
}

/// Return the component-wise minimum of two matrices.
template<typename DerivedLHS, typename DerivedRHS>
auto min(const MatrixBase<DerivedLHS>& lhs, const MatrixBase<DerivedRHS>& rhs) -> decltype(lhs.cwiseMin(rhs))
{
    return lhs.cwiseMin(rhs);
}

/// Return the maximum entry of a matrix.
template<typename Derived>
auto max(const MatrixBase<Derived>& mat) -> decltype(mat.maxCoeff())
{
    return mat.maxCoeff();
}

/// Return the component-wise maximum of two matrices.
template<typename DerivedLHS, typename DerivedRHS>
auto max(const MatrixBase<DerivedLHS>& lhs, const MatrixBase<DerivedRHS>& rhs) -> decltype(lhs.cwiseMax(rhs))
{
    return lhs.cwiseMax(rhs);
}

/// Return the component-wise multiplication of two matrices.
template<typename DerivedLHS, typename DerivedRHS>
auto operator%(const MatrixBase<DerivedLHS>& lhs, const MatrixBase<DerivedRHS>& rhs) -> decltype(lhs.cwiseProduct(rhs))
{
    return lhs.cwiseProduct(rhs);
}

/// Return the component-wise division of two matrices.
template<typename DerivedLHS, typename DerivedRHS>
auto operator/(const MatrixBase<DerivedLHS>& lhs, const MatrixBase<DerivedRHS>& rhs) -> decltype(lhs.cwiseQuotient(rhs))
{
    return lhs.cwiseQuotient(rhs);
}

/// Return the component-wise division of a scalar by a matrix.
template<typename Derived>
auto operator/(const typename Derived::Scalar& scalar, const MatrixBase<Derived>& mat) -> decltype(scalar*mat.cwiseInverse())
{
    return scalar*mat.cwiseInverse();
}

/// Return the component-wise addition of a scalar and a matrix.
template<typename Derived>
auto operator+(const typename Derived::Scalar& scalar, const MatrixBase<Derived>& mat) -> decltype((scalar + mat.array()).matrix())
{
    return (scalar + mat.array()).matrix();
}

/// Return the component-wise addition of a matrix and a scalar.
template<typename Derived>
auto operator+(const MatrixBase<Derived>& mat, const typename Derived::Scalar& scalar) -> decltype((scalar + mat.array()).matrix())
{
    return (scalar + mat.array()).matrix();
}

/// Return the component-wise subtraction of a scalar and a matrix.
template<typename Derived>
auto operator-(const typename Derived::Scalar& scalar, const MatrixBase<Derived>& mat) -> decltype((scalar - mat.array()).matrix())
{
    return (scalar - mat.array()).matrix();
}

/// Return the component-wise subtraction of a matrix and a scalar.
template<typename Derived>
auto operator-(const MatrixBase<Derived>& mat, const typename Derived::Scalar& scalar) -> decltype((mat.array() - scalar).matrix())
{
    return (mat.array() - scalar).matrix();
}

/// Return the component-wise absolute entries of a matrix.
template<typename Derived>
auto abs(const MatrixBase<Derived>& mat) -> decltype(mat.cwiseAbs())
{
    return mat.cwiseAbs();
}

/// Return the component-wise square root of a matrix.
template<typename Derived>
auto sqrt(const MatrixBase<Derived>& mat) -> decltype(mat.cwiseSqrt())
{
    return mat.cwiseSqrt();
}

/// Return the component-wise exponential of a matrix.
template<typename Derived>
auto pow(const MatrixBase<Derived>& mat, double power) -> decltype(mat.array().pow(power).matrix())
{
    return mat.array().pow(power).matrix();
}

/// Return the component-wise natural exponential of a matrix.
template<typename Derived>
auto exp(const MatrixBase<Derived>& mat) -> decltype(mat.array().exp().matrix())
{
    return mat.array().exp().matrix();
}

/// Return the component-wise natural log of a matrix.
template<typename Derived>
auto log(const MatrixBase<Derived>& mat) -> decltype(mat.array().log().matrix())
{
    return mat.array().log().matrix();
}

/// Return the component-wise log10 of a matrix.
template<typename Derived>
auto log10(const MatrixBase<Derived>& mat) -> decltype(mat.array().log10().matrix())
{
    return mat.array().log10().matrix();
}

template<typename Derived>
auto begin(MatrixBase<Derived>& vec) -> typename Derived::Scalar*
{
    EIGEN_STATIC_ASSERT_VECTOR_ONLY(Derived)
    return vec.size() ? &vec[0] : nullptr;
}

template<typename Derived>
auto begin(const MatrixBase<Derived>& vec) -> typename Derived::Scalar*
{
    EIGEN_STATIC_ASSERT_VECTOR_ONLY(Derived)
    return vec.size() ? &vec[0] : nullptr;
}

template<typename Derived>
auto end(MatrixBase<Derived>& vec) -> typename Derived::Scalar*
{
    EIGEN_STATIC_ASSERT_VECTOR_ONLY(Derived)
    return vec.size() ? begin(vec) + vec.size() : nullptr;
}

template<typename Derived>
auto end(const MatrixBase<Derived>& vec) -> typename Derived::Scalar*
{
    EIGEN_STATIC_ASSERT_VECTOR_ONLY(Derived)
    return vec.size() ? begin(vec) + vec.size() : nullptr;
}

} // namespace Eigen
