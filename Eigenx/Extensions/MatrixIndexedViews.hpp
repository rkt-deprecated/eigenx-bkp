// Eigenx is a extension of the Eigen library that aims to have simpler syntax and faster build times.
//
// Copyright (C) 2014-2017 Allan Leal
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Eigenx/Eigen/Core>

namespace Eigen {

template<typename Derived, typename Indices>
class MatrixRowsView;

template<typename Derived, typename Indices>
class MatrixRowsViewConst;

template<typename Derived, typename Indices>
class MatrixColsView;

template<typename Derived, typename Indices>
class MatrixColsViewConst;

template<typename Derived, typename Indices>
class MatrixSubView;

template<typename Derived, typename Indices>
class MatrixSubViewConst;

namespace internal {

template<typename Derived, typename Indices>
struct traits<MatrixRowsView<Derived, Indices>>
{
    typedef Dense StorageKind;
    typedef MatrixXpr XprKind;
    typedef typename Derived::StorageIndex StorageIndex;
    typedef typename Derived::Scalar Scalar;
    enum {
        Flags = ColMajor | (is_lvalue<Derived>::value ? LvalueBit : 0),
        RowsAtCompileTime = Derived::RowsAtCompileTime,
        ColsAtCompileTime = Derived::ColsAtCompileTime,
        MaxRowsAtCompileTime = Derived::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = Derived::MaxColsAtCompileTime,
    };
};

template<typename Derived, typename Indices>
struct traits<MatrixRowsViewConst<Derived, Indices>>
{
    typedef Dense StorageKind;
    typedef MatrixXpr XprKind;
    typedef typename Derived::StorageIndex StorageIndex;
    typedef typename Derived::Scalar Scalar;
    enum {
        Flags = ColMajor,
        RowsAtCompileTime = Derived::RowsAtCompileTime,
        ColsAtCompileTime = Derived::ColsAtCompileTime,
        MaxRowsAtCompileTime = Derived::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = Derived::MaxColsAtCompileTime,
    };
};

template<typename Derived, typename Indices>
struct traits<MatrixColsView<Derived, Indices>>
{
    typedef Dense StorageKind;
    typedef MatrixXpr XprKind;
    typedef typename Derived::StorageIndex StorageIndex;
    typedef typename Derived::Scalar Scalar;
    enum {
        Flags = ColMajor | (is_lvalue<Derived>::value ? LvalueBit : 0),
        RowsAtCompileTime = Derived::RowsAtCompileTime,
        ColsAtCompileTime = Derived::ColsAtCompileTime,
        MaxRowsAtCompileTime = Derived::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = Derived::MaxColsAtCompileTime,
    };
};

template<typename Derived, typename Indices>
struct traits<MatrixColsViewConst<Derived, Indices>>
{
    typedef Dense StorageKind;
    typedef MatrixXpr XprKind;
    typedef typename Derived::StorageIndex StorageIndex;
    typedef typename Derived::Scalar Scalar;
    enum {
        Flags = ColMajor,
        RowsAtCompileTime = Derived::RowsAtCompileTime,
        ColsAtCompileTime = Derived::ColsAtCompileTime,
        MaxRowsAtCompileTime = Derived::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = Derived::MaxColsAtCompileTime,
    };
};

template<typename Derived, typename Indices>
struct traits<MatrixSubView<Derived, Indices>>
{
    typedef Dense StorageKind;
    typedef MatrixXpr XprKind;
    typedef typename Derived::StorageIndex StorageIndex;
    typedef typename Derived::Scalar Scalar;
    enum {
        Flags = ColMajor | (is_lvalue<Derived>::value ? LvalueBit : 0),
        RowsAtCompileTime = Derived::RowsAtCompileTime,
        ColsAtCompileTime = Derived::ColsAtCompileTime,
        MaxRowsAtCompileTime = Derived::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = Derived::MaxColsAtCompileTime,
    };
};

template<typename Derived, typename Indices>
struct traits<MatrixSubViewConst<Derived, Indices>>
{
    typedef Dense StorageKind;
    typedef MatrixXpr XprKind;
    typedef typename Derived::StorageIndex StorageIndex;
    typedef typename Derived::Scalar Scalar;
    enum {
        Flags = ColMajor,
        RowsAtCompileTime = Derived::RowsAtCompileTime,
        ColsAtCompileTime = Derived::ColsAtCompileTime,
        MaxRowsAtCompileTime = Derived::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = Derived::MaxColsAtCompileTime,
    };
};

template<typename ArgType, typename Indices>
struct evaluator<MatrixRowsView<ArgType, Indices>> : evaluator_base<MatrixRowsView<ArgType, Indices>>
{
    typedef MatrixRowsView<ArgType, Indices> XprType;
    typedef typename XprType::CoeffReturnType CoeffReturnType;
    typedef typename XprType::Scalar Scalar;
    enum
    {
        CoeffReadCost = evaluator<ArgType>::CoeffReadCost,
        Flags = ColMajor,
    };

    evaluator(const MatrixRowsView<ArgType, Indices>& view)
    : m_view(view) {}

    auto coeffRef(Index row, Index col) -> Scalar& { return m_view.coeffRef(row, col); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_view.coeff(row, col); }

    MatrixRowsView<ArgType, Indices> m_view;
};

template<typename ArgType, typename Indices>
struct evaluator<MatrixRowsViewConst<ArgType, Indices>> : evaluator_base<MatrixRowsViewConst<ArgType, Indices>>
{
    typedef MatrixRowsViewConst<ArgType, Indices> XprType;
    typedef typename XprType::CoeffReturnType CoeffReturnType;
    typedef typename XprType::Scalar Scalar;
    enum
    {
        CoeffReadCost = evaluator<ArgType>::CoeffReadCost,
        Flags = ColMajor,
    };

    evaluator(const MatrixRowsViewConst<ArgType, Indices>& view)
    : m_view(view) {}

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_view.coeff(row, col); }

    MatrixRowsViewConst<ArgType, Indices> m_view;
};

template<typename ArgType, typename Indices>
struct evaluator<MatrixColsView<ArgType, Indices>> : evaluator_base<MatrixColsView<ArgType, Indices>>
{
    typedef MatrixColsView<ArgType, Indices> XprType;
    typedef typename XprType::CoeffReturnType CoeffReturnType;
    typedef typename XprType::Scalar Scalar;
    enum
    {
        CoeffReadCost = evaluator<ArgType>::CoeffReadCost,
        Flags = ColMajor,
    };

    evaluator(const MatrixColsView<ArgType, Indices>& view)
    : m_view(view) {}

    auto coeffRef(Index row, Index col) -> Scalar& { return m_view.coeffRef(row, col); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_view.coeff(row, col); }

    MatrixColsView<ArgType, Indices> m_view;
};

template<typename ArgType, typename Indices>
struct evaluator<MatrixColsViewConst<ArgType, Indices>> : evaluator_base<MatrixColsViewConst<ArgType, Indices>>
{
    typedef MatrixColsViewConst<ArgType, Indices> XprType;
    typedef typename XprType::CoeffReturnType CoeffReturnType;
    typedef typename XprType::Scalar Scalar;
    enum
    {
        CoeffReadCost = evaluator<ArgType>::CoeffReadCost,
        Flags = ColMajor,
    };

    evaluator(const MatrixColsViewConst<ArgType, Indices>& view)
    : m_view(view) {}

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_view.coeff(row, col); }

    MatrixColsViewConst<ArgType, Indices> m_view;
};

template<typename ArgType, typename Indices>
struct evaluator<MatrixSubView<ArgType, Indices>> : evaluator_base<MatrixSubView<ArgType, Indices>>
{
    typedef MatrixSubView<ArgType, Indices> XprType;
    typedef typename XprType::CoeffReturnType CoeffReturnType;
    typedef typename XprType::Scalar Scalar;
    enum
    {
        CoeffReadCost = evaluator<ArgType>::CoeffReadCost,
        Flags = ColMajor,
    };

    evaluator(const MatrixSubView<ArgType, Indices>& view)
    : m_view(view) {}

    auto coeffRef(Index row, Index col) -> Scalar& { return m_view.coeffRef(row, col); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_view.coeff(row, col); }

    MatrixSubView<ArgType, Indices> m_view;
};

template<typename ArgType, typename Indices>
struct evaluator<MatrixSubViewConst<ArgType, Indices>> : evaluator_base<MatrixSubViewConst<ArgType, Indices>>
{
    typedef MatrixSubViewConst<ArgType, Indices> XprType;
    typedef typename XprType::CoeffReturnType CoeffReturnType;
    typedef typename XprType::Scalar Scalar;
    enum
    {
        CoeffReadCost = evaluator<ArgType>::CoeffReadCost,
        Flags = ColMajor,
    };

    evaluator(const MatrixSubViewConst<ArgType, Indices>& view)
    : m_view(view) {}

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_view.coeff(row, col); }

    MatrixSubViewConst<ArgType, Indices> m_view;
};

} // namespace internal

template<typename Derived, typename Indices>
class MatrixRowsView : public MatrixBase<MatrixRowsView<Derived, Indices>>
{
public:
    typedef MatrixBase<MatrixRowsView> Base;
    typedef typename Derived::PlainObject PlainObject;
    EIGEN_DENSE_PUBLIC_INTERFACE(MatrixRowsView)

    MatrixRowsView(MatrixBase<Derived>& mat, const Indices& irows)
    : m_mat(mat.derived()), m_irows(irows) {}

    auto rows() const -> Index { return m_irows.size(); }
    auto cols() const -> Index { return m_mat.cols(); }

    auto coeffRef(Index row, Index col) -> Scalar& { return m_mat.coeffRef(m_irows[row], col); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_mat.coeff(m_irows[row], col); }

    template<typename DerivedOther>
    auto operator=(const MatrixBase<DerivedOther>& other) -> MatrixRowsView&
    {
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                coeffRef(i, j) = other(i, j);
        return *this;
    }

    operator PlainObject() const
    {
        PlainObject res(rows(), cols());
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                res(i, j) = coeff(i, j);
        return res;
    }


private:
    typedef typename internal::ref_selector<Derived>::non_const_type DerivedNested;
    DerivedNested m_mat;
    const Indices& m_irows;
};

template<typename Derived, typename Indices>
class MatrixRowsViewConst : public MatrixBase<MatrixRowsViewConst<Derived, Indices>>
{
public:
    typedef MatrixBase<MatrixRowsViewConst> Base;
    typedef typename Derived::PlainObject PlainObject;
    EIGEN_DENSE_PUBLIC_INTERFACE(MatrixRowsViewConst)

    MatrixRowsViewConst(const MatrixBase<Derived>& mat, const Indices& irows)
      : m_mat(mat.derived()), m_irows(irows)
    {}

    auto rows() const -> Index { return m_irows.size(); }
    auto cols() const -> Index { return m_mat.cols(); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_mat.coeff(m_irows[row], col); }

    operator PlainObject() const
    {
        PlainObject res(rows(), cols());
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                res(i, j) = coeff(i, j);
        return res;
    }

private:
    typedef typename internal::ref_selector<Derived>::type DerivedNested;
    DerivedNested m_mat;
    const Indices& m_irows;
};

template<typename Derived, typename Indices>
class MatrixColsView : public MatrixBase<MatrixColsView<Derived, Indices>>
{
public:
    typedef MatrixBase<MatrixColsView> Base;
    typedef typename Derived::PlainObject PlainObject;
    EIGEN_DENSE_PUBLIC_INTERFACE(MatrixColsView)

    MatrixColsView(MatrixBase<Derived>& mat, const Indices& icols)
      : m_mat(mat.derived()), m_icols(icols)
    {}

    template<typename DerivedOther>
    auto operator=(const MatrixBase<DerivedOther>& other) -> MatrixColsView&
    {
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                coeffRef(i, j) = other(i, j);
        return *this;
    }

    auto rows() const -> Index { return m_mat.rows(); }
    auto cols() const -> Index { return m_icols.size(); }

    auto coeffRef(Index row, Index col) -> Scalar& { return m_mat.coeffRef(row, m_icols[col]); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_mat.coeff(row, m_icols[col]); }

    operator PlainObject() const
    {
        PlainObject res(rows(), cols());
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                res(i, j) = coeff(i, j);
        return res;
    }

private:
    typedef typename internal::ref_selector<Derived>::non_const_type DerivedNested;
    DerivedNested m_mat;
    const Indices& m_icols;
};

template<typename Derived, typename Indices>
class MatrixColsViewConst : public MatrixBase<MatrixColsViewConst<Derived, Indices>>
{
public:
    typedef MatrixBase<MatrixColsViewConst> Base;
    typedef typename Derived::PlainObject PlainObject;
    EIGEN_DENSE_PUBLIC_INTERFACE(MatrixColsViewConst)

    MatrixColsViewConst(const MatrixBase<Derived>& mat, const Indices& icols)
      : m_mat(mat.derived()), m_icols(icols)
    {}

    auto rows() const -> Index { return m_mat.rows(); }
    auto cols() const -> Index { return m_icols.size(); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_mat.coeff(row, m_icols[col]); }

    operator PlainObject() const
    {
        PlainObject res(rows(), cols());
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                res(i, j) = coeff(i, j);
        return res;
    }

private:
    typedef typename internal::ref_selector<Derived>::type DerivedNested;
    DerivedNested m_mat;
    const Indices& m_icols;
};

template<typename Derived, typename Indices>
class MatrixSubView : public MatrixBase<MatrixSubView<Derived, Indices>>
{
public:
    typedef MatrixBase<MatrixSubView> Base;
    typedef typename Derived::PlainObject PlainObject;
    EIGEN_DENSE_PUBLIC_INTERFACE(MatrixSubView)

    MatrixSubView(MatrixBase<Derived>& mat, const Indices& irows, const Indices& icols)
      : m_mat(mat.derived()), m_irows(irows), m_icols(icols)
    {}

    template<typename DerivedOther>
    auto operator=(const MatrixBase<DerivedOther>& other) -> MatrixSubView&
    {
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                coeffRef(i, j) = other(i, j);
        return *this;
    }

    auto rows() const -> Index { return m_irows.size(); }
    auto cols() const -> Index { return m_icols.size(); }

    auto coeffRef(Index row, Index col) -> Scalar& { return m_mat.coeffRef(m_irows[row], m_icols[col]); }

    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_mat.coeff(m_irows[row], m_icols[col]); }

    operator PlainObject() const
    {
        PlainObject res(rows(), cols());
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                res(i, j) = coeff(i, j);
        return res;
    }

private:
    typedef typename internal::ref_selector<Derived>::non_const_type DerivedNested;
    DerivedNested m_mat;
    const Indices& m_irows;
    const Indices& m_icols;
};

template<typename Derived, typename Indices>
class MatrixSubViewConst : public MatrixBase<MatrixSubViewConst<Derived, Indices>>
{
public:
    typedef MatrixBase<MatrixSubViewConst> Base;
    typedef typename Derived::PlainObject PlainObject;
    EIGEN_DENSE_PUBLIC_INTERFACE(MatrixSubViewConst)

    MatrixSubViewConst(const MatrixBase<Derived>& mat, const Indices& irows, const Indices& icols)
      : m_mat(mat.derived()), m_irows(irows), m_icols(icols)
    {}

    auto rows() const -> Index { return m_irows.size(); }
    auto cols() const -> Index { return m_icols.size(); }

    auto coeffRef(Index row, Index col) const -> const Scalar& { return m_mat.coeffRef(m_irows[row], m_icols[col]); }
    auto coeff(Index row, Index col) const -> CoeffReturnType { return m_mat.coeff(m_irows[row], m_icols[col]); }

    operator PlainObject() const
    {
        PlainObject res(rows(), cols());
        for(Index i = 0; i < rows(); ++i)
            for(Index j = 0; j < cols(); ++j)
                res(i, j) = coeff(i, j);
        return res;
    }

private:
    typedef typename internal::ref_selector<Derived>::type DerivedNested;
    DerivedNested m_mat;
    const Indices& m_irows;
    const Indices& m_icols;
};

/// Return a view of some rows of a matrix with given indices
/// @param mat The matrix for which the view is created
/// @param irows The indices of the rows of the matrix
template<typename Derived, typename Indices>
auto rows(MatrixBase<Derived>& mat, const Indices& irows) -> MatrixRowsView<Derived, Indices>
{
    return {mat, irows};
}

/// Return a const view of some rows of a matrix with given indices
/// @param mat The matrix for which the view is created
/// @param irows The indices of the rows of the matrix
template<typename Derived, typename Indices>
auto rows(const MatrixBase<Derived>& mat, const Indices& irows) -> MatrixRowsViewConst<Derived, Indices>
{
    return {mat, irows};
}

/// Return a view of some columns of a matrix with given indices
/// @param mat The matrix for which the view is created
/// @param icols The indices of the columns of the matrix
template<typename Derived, typename Indices>
auto cols(MatrixBase<Derived>& mat, const Indices& icols) -> MatrixColsView<Derived, Indices>
{
    return {mat, icols};
}

/// Return a const view of some columns of a matrix with given indices
/// @param mat The matrix for which the view is created
/// @param icols The indices of the columns of the matrix
template<typename Derived, typename Indices>
auto cols(const MatrixBase<Derived>& mat, const Indices& icols) -> MatrixColsViewConst<Derived, Indices>
{
    return {mat, icols};
}

/// Return a view of some rows and columns of a matrix with given indices
/// @param mat The matrix for which the view is created
/// @param irows The indices of the rows of the matrix
/// @param icols The indices of the columns of the matrix
template<typename Derived, typename Indices>
auto submatrix(MatrixBase<Derived>& mat, const Indices& irows, const Indices& icols) -> MatrixSubView<Derived, Indices>
{
    return {mat, irows, icols};
}

/// Return a const view of some rows and columns of a matrix with given indices
/// @param mat The matrix for which the view is created
/// @param irows The indices of the rows of the matrix
/// @param icols The indices of the columns of the matrix
template<typename Derived, typename Indices>
auto submatrix(const MatrixBase<Derived>& mat, const Indices& irows, const Indices& icols) -> MatrixSubViewConst<Derived, Indices>
{
    return {mat, irows, icols};
}

} // namespace Eigen
