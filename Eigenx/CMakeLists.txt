# Create an install target for Eigenx header files
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    DESTINATION "include" COMPONENT headers
    FILES_MATCHING PATTERN "*.hpp" PATTERN "*.hxx")

# Create an install target for Eigen header files
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/Eigen
    DESTINATION "include/Eigenx" COMPONENT headers)
