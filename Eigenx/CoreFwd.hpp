// Eigenx is a extension of the Eigen library that aims to have simpler syntax and faster build times.
//
// Copyright (C) 2014-2017 Allan Leal
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef EIGEN_DEFAULT_TO_ROW_MAJOR
#define EIGEN_MATRIX_STORAGE_ORDER_OPTION 0x1
#else
#define EIGEN_MATRIX_STORAGE_ORDER_OPTION 0
#endif

namespace Eigen {

template<typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
class Matrix;

template<typename PlainObjectType, int Options, typename StrideType>
class Ref;

template<typename PlainObjectType, int MapOptions, typename StrideType>
class Map;

using MatrixXi = Matrix<int, -1, -1, EIGEN_MATRIX_STORAGE_ORDER_OPTION, -1, -1>;
using MatrixXf = Matrix<float, -1, -1, EIGEN_MATRIX_STORAGE_ORDER_OPTION, -1, -1>;
using MatrixXd = Matrix<double, -1, -1, EIGEN_MATRIX_STORAGE_ORDER_OPTION, -1, -1>;

using VectorXi = Matrix<int, -1, 1, EIGEN_MATRIX_STORAGE_ORDER_OPTION, -1, 1>;
using VectorXf = Matrix<float, -1, 1, EIGEN_MATRIX_STORAGE_ORDER_OPTION, -1, 1>;
using VectorXd = Matrix<double, -1, 1, EIGEN_MATRIX_STORAGE_ORDER_OPTION, -1, 1>;

}
